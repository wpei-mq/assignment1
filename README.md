## Q. Is the Credit solution better or worse than the Pass solution? Why?

With the current requirements, neither solution is better or worse than the other due to the fact that both solutions have the same number of calculations for calculating the random cell surface type. Furthermore, if more types of surfaces need to be added, it is easily managed with either solution, tho the Credit solution is more elegant in structure (additional cases would need to be added to the switch statement and the random number bounds increased accordingly). 

However, if additional functionality needed to be added onto the Cells, or to individual surface types, it is much more easily done with the Credit solution. Since the Cell class is now abstract, and each surface type is a subclass of Cell, any changes made to the Cell class's properties are inherited by it's subclasses, and when a surface type needs an unique property, a change can be made to that surface type without affecting other subclasses and the superclass. 

Taking this into consideration, the Credit solution ends up being the better solution, allowing for easier implementation of future functionality. 