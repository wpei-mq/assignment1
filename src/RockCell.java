import java.awt.*;

public class RockCell extends Cell {

    public RockCell(int x, int y) {
        super(x, y);
    }

    @Override
    public void paint(Graphics g) {
        c = new Color(182, 182, 182); //gray
        g.setColor(c);
        g.fillRect(x, y, 35, 35);
    }
}
