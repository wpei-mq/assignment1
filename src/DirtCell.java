import java.awt.*;

public class DirtCell extends Cell {

    public DirtCell(int x, int y) {
        super(x, y);
    }

    @Override
    public void paint(Graphics g) {
        c = new Color(102, 51, 0); //brown
        g.setColor(c);
        g.fillRect(x, y, 35, 35);
    }
}
