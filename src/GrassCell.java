import java.awt.*;

public class GrassCell extends Cell {

    public GrassCell(int x, int y) {
        super(x, y);
    }

    @Override
    public void paint(Graphics g) {
        c = new Color(102, 153, 0); //light green
        g.setColor(c);
        g.fillRect(x, y, 35, 35);
    }
}
