import java.awt.*;
import java.util.Random;

public class PassCell {

    int x;
    int y;
    Random r = new Random();
    int rand;
    Color c;

    public PassCell(int x, int y) {
        this.x = x;
        this.y = y;
        rand = r.nextInt(4);
    }

    public void paint(Graphics g, Boolean highlighted) {
        if (highlighted) {
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(x, y, 35, 35);
        }

        switch (rand) {
            case 0:
                c = new Color(102, 51, 0); //brown
                g.setColor(c);
                g.fillRect(x, y, 35, 35);
                break;
            case 1:
                c = new Color(102, 153, 0); //light green
                g.setColor(c);
                g.fillRect(x, y, 35, 35);
                break;
            case 2:
                c = new Color(0, 102, 0); //dark green
                g.setColor(c);
                g.fillRect(x, y, 35, 35);
                break;
            case 3:
                c = new Color(182, 182, 182); //gray
                g.setColor(c);
                g.fillRect(x, y, 35, 35);
                break;
        }
    }

    public boolean contains(Point target){
        if (target == null)
            return false;
        return target.x > x && target.x < x + 35 && target.y > y && target.y < y +35;
    }
}
