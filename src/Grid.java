import java.awt.*;

public class Grid {

    private Cell[][] cells  = new Cell[20][20];

    public Grid(int x, int y) {
        // Credit level
        // Random r = new Random();

        // Generate a new noise map to map the cells to
        SimplexNoise simplexNoise = new SimplexNoise(200, 1);

        for(int i = 0; i < 20; i++) {
            for(int j = 0; j < 20; j++) {
                // Get the noise value for each cell, then choose which cell to paint
                double noise = 0.5 * (1 + simplexNoise.getNoise((x + i * 35), y + j * 35));

                if (noise > 1.5) {
                    cells[i][j] = new RockCell(x + i * 35, y + j * 35);
                } else if (noise > 1 && noise <= 1.5) {
                    cells[i][j] = new TreeCell(x + i * 35, y + j * 35);
                } else if (noise > 0 && noise <= 1) {
                    cells[i][j] = new GrassCell(x + i * 35, y + j * 35);
                } else {
                    cells[i][j] = new DirtCell(x + i * 35, y + j * 35);
                }

                /* Credit Level Solution
                 * Generates a random number between 0 and 3
                 * Assign a type of cell depending on what number is generated
                int rand = r.nextInt(4);

                switch(rand) {
                    case 0:
                        cells[i][j] = new RockCell(x + j * 35, y + i * 35);
                        break;
                    case 1:
                        cells[i][j] = new TreeCell(x + j * 35, y + i * 35);
                        break;
                    case 2:
                        cells[i][j] = new GrassCell(x + j * 35, y + i * 35);
                        break;
                    case 3:
                        cells[i][j] = new DirtCell(x + j * 35, y + i * 35);
                }
                 */
            }
        }
    }

    public void paint(Graphics g, Point mousePosition) {
        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 20; ++x) {
                Cell thisCell = cells[x][y];
                thisCell.paint(g);
                thisCell.paint(g, thisCell.contains(mousePosition));
            }
        }
    }
}
