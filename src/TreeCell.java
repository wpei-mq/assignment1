import java.awt.*;

public class TreeCell extends Cell {

    public TreeCell(int x, int y) {
        super(x, y);
    }

    @Override
    public void paint(Graphics g) {
        c = new Color(0, 102, 0); //dark green
        g.setColor(c);
        g.fillRect(x, y, 35, 35);
    }
}
