import java.awt.*;

public abstract class Cell {

    int x;
    int y;
    Color c;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics g, Boolean highlighted) {
        // Creates a fake border around the highlighted cell
        if (highlighted) {
            g.setColor(Color.BLACK);
            g.drawRect(x, y, 34, 34);
            g.drawRect(x + 1, y + 1, 32, 32);
        }
    }

    public boolean contains(Point target){
        if (target == null)
            return false;
        return target.x > x && target.x < x + 35 && target.y > y && target.y < y +35;
    }

    public abstract void paint(Graphics g);
}
